'''
Created on Mar 27, 2019

@author: robzo
'''

#problem 3.1

def intpow(i,n):
    #recursive function for calculating powers of a number
    
    #base case
    if n == 0:
        return 1
    
    #recursive case
    result = intpow(i, n-1)*i
    return result

print("expecting 16")
print(intpow(2,4))

print("expecting 27")
print(intpow(3,3))
