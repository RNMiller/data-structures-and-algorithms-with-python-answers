'''
Created on Mar 27, 2019

@author: robzo
'''

#3.1

def recurStrLen(s):
    #recusive function to find string length without using len()
    
    if s == '':
        return 0
    
    length = recurStrLen(s[1:])+1
    return length
    
#expect hello to be length 5
print(recurStrLen('hello'))

#expect Peter Piper Picked a Peck of Pickled Peppers to be 44 long
print(recurStrLen('Peter Piper Picked a Peck of Pickled Peppers'))