'''
Created on Mar 27, 2019

@author: robzo
'''
#problem 3.4

def swap(s):
    #takes an input string and swaps every two letters
    
    #base case
    if s == '' or len(s) == 1:
        return s
    
    #recursive case
    result = s[1] + s[0]
    
    return result + swap(s[2:])

print('expect: badcfehg')
print(swap('abcdefgh'))

print('expect: eHll ohTre,eG nerelaK neboi')
print(swap('Hello There, General Kenobi'))
