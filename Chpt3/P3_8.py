'''
Created on Mar 28, 2019

@author: robzo
'''
#3.8
#create a forward running version of the reverse list function

def revListForward(lst):
    #reverses the list using a helper function
    
    def revListHelper2(index):
        #recursively reverses a list front to back
        
        if index == len(lst):
            return []
        
        restRev = revListHelper2(index+1)
        last = [lst[index]]
        
        result = restRev + last
        
        return result
    #return the correct type
    if type(lst) == list:
        return revListHelper2(0)
    return ''.join(+revListHelper2(0))
    
print(revListForward('hello'))