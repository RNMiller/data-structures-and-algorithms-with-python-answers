'''
Created on Mar 27, 2019

@author: robzo
'''
#3.5
#create a recusive function to draw a tree with a turtle

import turtle

t = turtle.Turtle()
t.pencolor("brown")
turtle.setup(1000, 800, 0 , 0)
LMULTI = 10

def drawBranch(t, branches, angle):
    #draws branches that slowly get shorter and narrower
    
    #base case, draws a leaf
    if branches == 0:
        t.pencolor("green")
        t.shape("square")
        t.width(10)
        t.pendown()
        t.fd(10)
        t.penup()
        t.bk(10)
        t.shape("circle")
        t.pencolor("brown")
        return
        
    #recursive case, draws a branch
    t.width(branches*3)
    t.fd(branches*LMULTI*2)
    t.pendown()
    t.lt(angle//2)
    t.width(branches*2)
    #call the recursive function
    drawBranch(t, branches-1, angle)
    #get back to the initial position and draw the other branch
    t.penup()
    t.width(branches*2)
    t.rt(angle)
    t.pendown()
    #call the recursive function again
    drawBranch(t, branches-1, angle)
    #get back to our starting point
    t.penup()
    t.lt(angle//2)
    t.bk(branches*LMULTI*2)
    
    
def drawTree(t, branches, angle):
    #sets up the tree to look pretty
    t.penup()
    t.lt(90)
    t.bk(300)
    t.pendown()
    drawBranch(t, branches, angle)
    
    
drawTree(t, 7, 60)
turtle.mainloop()