'''
Class for a priority queue where the imput are tuples
Created on Apr 6, 2019

@author: robzo
'''

from queue import Queue

class PriorityQueue(Queue):
    class __Node:
        '''the individual node of the linked list
        getItem() returns item
        setItem(value) sets item
        getNext() returns next list item
        setNext(next)'''
        def __init__(self, item, priority=0, next=None):
            self.item = item
            self.next = next
            self.priority = priority
        
        def getItem(self):
            return self.item
        
        def getNext(self):
            return self.next
        
        def getPriority(self):
            return self.priority
        
        def setItem(self, item):
            self.item = item
            
        def setNext(self, next):
            self.next = next   
        
        
    def __init__(self,contents=[], priorities=[]):
        #creates a single empty value to avoid weirdness later
        self.first = PriorityQueue.__Node(None,0,None)
        self.last = self.first
        self.numItems = 0
        #put the passed items into the list
        for e in contents:
            self.__append(e, priorities[e])
            
    def __append(self, val, priority):
        #appends the item to the linked list
        node = PriorityQueue.__Node(val, priority)
        self.last.setNext(node)
        self.last = node
        self.numItems += 1
            
    def enqueue(self, val, priority):
        #adds a new value at the end of it's current priority
        cursor = self.first
        
        #find the last item of the current priority
        while(cursor.getNext() != None and 
              cursor.getNext().getPriority() <= priority):
            cursor = cursor.getNext()
        #if we reached the end of the list, append the value
        if cursor.getNext() == None:
            node = PriorityQueue.__Node(val, priority)
            self.last.setNext(node)
            self.last = node
            self.numItems += 1
        
        #if we reached the end of our current priority, insert the value
        else:
            node = PriorityQueue.__Node(val, priority, cursor.getNext())
            cursor.setNext(node)
            self.numItems += 1
            
def main():
    qu = PriorityQueue(range(10), range(10))
    print(qu.numItems)
    
    qu.enqueue(4,2)
    print(qu.numItems)
    qu.enqueue(11, 1)
    
    for i in range(5):
        print(qu.dequeue())
    
    
    
if __name__ == "__main__":
    main()        
            
        
            