'''
Chapt 4 Problem 1, comparing different sorts using PyList
Created on Apr 3, 2019

@author: robzo
'''

from Pylist import PyList
import random, copy, time

class Qsequence(PyList):
    '''PyList that allows sorting'''
    def __randomize(self):
        #randomizes the list for sorting purposes
        for i in range(self.numItems):
            j = random.randint(0, self.numItems-1)
            tmp = self.items[i]
            self.items[i] = self.items[j]
            self.items[j] = tmp
        return 
    
    def partition(self, start, stop):
        #pivotindex comes from the start location of the list
        pivotIndex = start
        pivot = self.items[pivotIndex]
        i = start + 1
        j = stop - 1
        
        while i <= j:
            while i <= j and not pivot < self.items[i]:
                i+=1
            while i <= j and pivot < self.items[j]:
                j-=1
                
            if i < j:
                tmp = self.items[i]
                self.items[i] = self.items[j]
                self.items[j] = tmp
                i+=1
                j-=1
        self.items[pivotIndex] = self.items[j]
        self.items[j] = pivot
        
        return j
    
    def __quicksortRecursively(self, start, stop):
        if start >= stop-1:
            return
        
        pivotIndex = self.partition(start, stop)
        
        self.__quicksortRecursively(start, pivotIndex)
        self.__quicksortRecursively(pivotIndex+1, stop)
        
    
    def Qsort(self):
        #sorts the list recursively using quicksort algorithm
        self.__randomize()
        self.__quicksortRecursively(0, self.numItems)
        
class MSequence(PyList):
    '''PyList with merge sorting'''
    def __merge(self, start, mid, stop):
        lst = []
        i = start
        j = mid
        
        #merge 2 lists while each has more elements
        while i< mid and j < stop:
            if self.items[i] < self.items[j]:
                lst.append(self.items[i])
                i+=1
            else:
                lst.append(self.items[j])
                j+=1
        
        #copy in the rest of the start to mid sequence
        while i < mid:
            lst.append(self.items[i])
            i+=1
            
        for i in range(len(lst)):
            self.items[start+i]=lst[i]
            
    def __mergeSortRecursively(self, start, stop):
        if start >= stop-1:
            return
        
        mid = (start + stop)//2
        
        self.__mergeSortRecursively(start, mid)
        self.__mergeSortRecursively(mid, stop)
        self.__merge(start, mid, stop)
        
    def mergeSort(self):
        self.__mergeSortRecursively(0, self.numItems)
        

    
    
def testSortTimes():
    '''Creates an XML comparing different sorting methods'''
    file = open("SortMethodTimings", "w")
    
    file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    
    file.write('<Plot title="Sorting Times vs Number of Items">\n')
    
    #size of lists to use
    xmin = 1000
    xmax = 2000
    xstep = 100
    
    #record the list sizes in x and the sort times in y
    xlist = []
    baseList = []
    Qlist = []
    Mlist = []
    Plist = []
    
    #make a random list of integers in baselist
    for i in range(0,xmin):
        baseList.append(random.randint(0,xmax))
    
    for x in range(xmin, xmax+1, xstep):
        #create the same random list for each list
        xlist.append(x)
        Ml = MSequence(baseList)
        Ql = Qsequence(baseList)
        Pl = copy.deepcopy(baseList)
        
        time.sleep(1)
        
        #test Mlist time and record it in the list
        starttime = time.perf_counter()
        Ml.mergeSort()
        endtime = time.perf_counter()
        deltaT = endtime - starttime
        Mlist.append(deltaT*1000000)
        
        #test Qlist time and record it in the list
        starttime = time.perf_counter()
        Ql.Qsort()
        endtime = time.perf_counter()
        deltaT = endtime - starttime
        Qlist.append(deltaT*1000000)
        
        #test build in list sort time and record it in the list
        starttime = time.perf_counter()
        Pl.sort()
        endtime = time.perf_counter()
        deltaT = endtime - starttime
        Plist.append(deltaT*1000000)
        
        #increase the list size
        for i in range(0,xstep):
            baseList.append(random.randint(0,xmax))
        
    file.write('   <Axes>\n')
    file.write('   <XAxis min="'+str(xmin)+'" max="'+str(xmax)+'">List Size</XAxis>\n')
    file.write('   <YAxis min="'+str(min(Plist))+'"max="'+str(max(Mlist)+2000)+'">Microseconds</YAxis>\n')
    file.write('   </Axes>\n')
    
    #write the merge sort values
    file.write('   <Sequence title="Merge Sort Time vs List Size" color="red">\n')
    
    for i in range(len(xlist)):
        file.write('   <DataPoint x="'+str(xlist[i])+'"y="'+str(Mlist[i])+'"/>\n')
    
    file.write('   <Sequence>\n')
    
    #write the quick sort values
    file.write('   <Sequence title="Quick Sort Time vs List Size" color="blue">\n')
    
    for i in range(len(xlist)):
        file.write('   <DataPoint x="'+str(xlist[i])+'"y="'+str(Qlist[i])+'"/>\n')
    
    file.write('   <Sequence>\n')
    
    #write the built in sort values
    file.write('   <Sequence title="Built in Sort Time vs List Size" color="red">\n')
    
    for i in range(len(xlist)):
        file.write('   <DataPoint x="'+str(xlist[i])+'"y="'+str(Plist[i])+'"/>\n')
    
    file.write('   <Sequence>\n')
    file.write('</Plot>\n')
    print('done')
    file.close
        
        

'''    
pl = Qsequence([2, 10, 5, 12, 2, 4, 8], 10)
print(pl)
pl.Qsort()
print(pl.items)
ml = MSequence([2, 10, 5, 12, 2, 4, 8], 10)
print('m')
print(ml)
ml.mergeSort()
print(ml)
nl = PyList([2, 10, 5, 12, 2, 4, 8], 7)
print('n')
print(nl)
nl.items.sort()
print(nl)
'''
testSortTimes()


