'''
Impliments the the linked list data format
Created on Apr 5, 2019

@author: robzo
'''


class LinkedList:
    '''List where each member has a value and a pointer to the next value
    Use like a normal list, except it cannot sort'''
    
    class __Node:
        '''the individual node of the linked list
        getItem() returns item
        setItem(value) sets item
        getNext() returns next list item
        setNext(next)'''
        def __init__(self, item, next=None):
            self.item = item
            self.next = next
        
        def getItem(self):
            return self.item
        
        def getNext(self):
            return self.next
        
        def setItem(self, item):
            self.item = item
            
        def setNext(self, next):
            self.next = next
            
    def __init__(self,contents=[]):
        #creates a single empty value to avoid weirdness later
        self.first = LinkedList.__Node(None,None)
        self.last = self.first
        self.numItems = 0
        #put the passed items into the list
        for e in contents:
            self.append(e)
            
    def __getitem__(self, index):
        #go through the list to find the item we need
        if index >= 0 and index < self.numItems:
            cursor = self.first.getNext()
            for i in range(index):
                cursor = cursor.getNext()
                
            return cursor.getItem()
        
        raise IndexError("LinkedList index out of range")
    
    def __setitem__(self, index, val):
        #go through the list and set the value we want
        if index>= 0 and index < self.numItems:
            cursor = self.first.getNext()
            for i in range(index):
                cursor = cursor.getNext()
                
            cursor.setItem(val)
            return 
        
        raise IndexError("LinkedList assignment index out of range")  
    
    def __add__(self, other):
        #check for type errors
        if type(self) != type(other):
            raise TypeError("Concatenate undefined for " + str(type(self)) + " + " +
                            str(type(other)))
            
        result = LinkedList()
        
        #put self into the result list
        cursor = self.first.getNext()
        while cursor != None:
            result.append(cursor.getItem())
            cursor = cursor.getNext()
        
        #put other into the result list
        cursor = other.first.getNext()
        while cursor != None:
            result.append(cursor.getItem())
            cursor = cursor.getNext()
            
        return result
    
    def append(self, val):
        #appends the item to the linked list
        node = LinkedList.__Node(val)
        self.last.setNext(node)
        self.last = node
        self.numItems += 1
        
    def insert(self, index, val):
        #inserts an item into the linked list
        cursor = self.first
        
        if index < self.numItems:
            for i in range(index):
                cursor = cursor.getNext()
                
            node = LinkedList.__Node(val, cursor.getNext())
            cursor.setNext(node)
            self.numItems += 1
        else:
            self.append(val)
            
    def __delitem__(self, index):
        #removes an index from the linked list
        if index >= 0 and index < self.numItems:
            cursor = self.first
            
            for i in range(index):
                cursor = cursor.getNext()
                
            #change the reference for the next item
            cursor.setNext( cursor.getNext().getNext() )
            self.numItems -= 1
            return
            
        
        raise IndexError("LinkedList index out of range")
    
    def __eq__(self, other):
        #test equality between two lists
        #test for type
        if type(self) != type(other):
            return False
        
        #test for length of list
        if self.numItems != other.numItems:
            return False
            
        cursorA = self.first
        cursorB = other.first
        
        #test each member
        for i in range(self.numItems):
            cursorA = cursorA.getNext()
            cursorB = cursorB.getNext()
            if cursorA.getItem() != cursorB.getItem():
                return False
        
        return True
    
    def __iter__(self):
        #iterate through the list and yield each item
        cursor = self.first
        for i in range(self.numItems):
            cursor = cursor.getNext()
            yield cursor.getItem()  
            
    def __len__(self):
        #returns the number of items in the list
        return self.numItems  
    
    def __contains__(self, val):
        #tests if a value is contained in the list
        for i in self:
            if i == val:
                return True
        return False
    
    
def main():        
    lst = LinkedList([8, 12, 15])
    print("Expect 3")
    print(lst.numItems)
    del lst[0]
    print("Expect 2")
    print(lst.numItems)
    lst2 = LinkedList([2, 5, 300])
    lst = lst + lst2
    print("expect 5")
    print(lst.numItems)
    print("expect true, then false")
    print(12 in lst)
    print(80 in lst)
    print("expect 5")
    print(len(lst))
    print("expect full list printed")
    for i in lst:
        print(i)
    
if __name__ == "__main__":
    main()
