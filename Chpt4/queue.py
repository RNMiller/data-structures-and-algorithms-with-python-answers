'''
UsesFIFO queue
Created on Apr 6, 2019

@author: robzo
'''
class Queue:
    '''List where each member has a value and a pointer to the next value
    Use like a normal list, except it cannot sort'''
    
    class __Node:
        '''the individual node of the linked list
        getItem() returns item
        setItem(value) sets item
        getNext() returns next list item
        setNext(next)'''
        def __init__(self, item, next=None):
            self.item = item
            self.next = next
        
        def getItem(self):
            return self.item
        
        def getNext(self):
            return self.next
        
        def setItem(self, item):
            self.item = item
            
        def setNext(self, next):
            self.next = next
            
    def __init__(self,contents=[]):
        #creates a single empty value to avoid weirdness later
        self.first = Queue.__Node(None,None)
        self.last = self.first
        self.numItems = 0
        #put the passed items into the list
        for e in contents:
            self.__append(e)
            
    def __getitem__(self, index):
        #go through the list to find the item we need
        if index >= 0 and index < self.numItems:
            cursor = self.first.getNext()
            for i in range(index):
                cursor = cursor.getNext()
                
            return cursor.getItem()
        
        raise IndexError("LinkedList index out of range")
        
    
    def __append(self, val):
        #appends the item to the linked list
        node = Queue.__Node(val)
        self.last.setNext(node)
        self.last = node
        self.numItems += 1
        
            
    def __delete(self, index):
        #removes an index from the linked list
        if index >= 0 and index < self.numItems:
            cursor = self.first
            
            for i in range(index):
                cursor = cursor.getNext()
                
            #change the reference for the next item
            cursor.setNext( cursor.getNext().getNext() )
            self.numItems -= 1
            return
            
        
        raise IndexError("LinkedList index out of range")
    
    def __eq__(self, other):
        #test equality between two lists
        #test for type
        if type(self) != type(other):
            return False
        
        #test for length of list
        if self.numItems != other.numItems:
            return False
            
        cursorA = self.first
        cursorB = other.first
        
        #test each member
        for i in range(self.numItems):
            cursorA = cursorA.getNext()
            cursorB = cursorB.getNext()
            if cursorA.getItem() != cursorB.getItem():
                return False
        
        return True
    
    def __iter__(self):
        #iterate through the list and yield each item
        cursor = self.first
        for i in range(self.numItems):
            cursor = cursor.getNext()
            yield cursor.getItem()  
            
    def __len__(self):
        #returns the number of items in the list
        return self.numItems  
    
    def __contains__(self, val):
        #tests if a value is contained in the list
        for i in self:
            if i == val:
                return True
        return False
    
    def dequeue(self):
        if self.numItems <= 0:
            raise IndexError("Cannot pop an empty queue")
        cursor = self.first
        val = cursor.getNext().getItem()    
                        
        #change the reference for the next item
        cursor.setNext( cursor.getNext().getNext() )
        self.numItems -= 1
        return val
    
    def enqueue(self, val):
        self.__append(val)
        
    def front(self):
        return self.first.getNext().getItem()
    
    def isEmpty(self):
        if self.numItems == 0:
            return True
        return False
    
    

def main():
    qu = Queue(range(10))
    print(qu.numItems)
    
    qu.enqueue(11)
    print(qu.numItems)
    
    for i in range(11):
        print(qu.dequeue())
    print(qu.numItems)
    
if __name__ == "__main__":
    main()
            
        
        
    


    