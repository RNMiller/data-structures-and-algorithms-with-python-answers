'''
Created on Mar 25, 2019

@author: robzo
'''

import time


class Clearable:
    #this class contains a fixed size list that can be cleared, appended
    #and searched
    def __init__(self, listMax):
        self.listMax = listMax
        self.fixedList = []
        self.index = 0
        for i in range(listMax):
            self.fixedList.append(None)
            
    def append(self, newItem):
    #special append for this object that clears the list if the list is
    #filled up
        if self.index == self.listMax:
            #set all members of the list to None
            for i in range(self.listMax):
                self.fixedList[i] = None
            self.index = 0
        #set the current index to the new item and increment index
        self.fixedList[self.index] = newItem
        self.index += 1
        
    def __getitem__(self, item):
        #searches the list and returns it if the item is found, returns
        #none otherwise
        for i in range(self.listMax):
            if self.fixedList[i] == item:
                return item
        return None
  
  
#test function for speed of appending this list
#I suspect that it will be an O(1) complexity, because although the 
#append function must set everything to None, it is done with frequency
#inversely proportional to the length of the list
def test():
    #write an xml file with the results
    file = open("FixedListCompareTiming.xml", "w")
    
    file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    
    file.write('<Plot title="Fixed Index append time for Random Lists">\n')
    
    #min and max list lengths
    listMin = 1000
    listMax = 20001
    
    xList = []
    yList = []
    
    #loop to create a clearable list of length 50, and add values to it
    for i in range(listMin, listMax, 1000):
        #create the list item
        cl = Clearable(50)
        
        xList.append(i)
        
        #allow time for garbage collection
        time.sleep(1)
        
        startTime = time.perf_counter()
        #append the test number of items to the list
        for j in range(i):
            cl.append(1)
            
        endTime = time.perf_counter()
        
        deltaT = (endTime - startTime)
        
        yList.append(deltaT*1000000)
        '''
        #append the number of items 
        for j in range(1000):
            #create a random int in range of the list
            x = random.randint(0,i-1)
            startTime = time.perf_counter()
            #dummy assignment
            y = findInList(x, indList)
            endTime = time.perf_counter()
            deltaT = (endTime - startTime)
            #add the time taken to the total time
            totalTime += deltaT
        '''
        
    #add all this info to our xml file
    
    file.write('  <Sequence title="Time for append for 50 item list" color="blue"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    
    #redo the test with 500 list
    xList = []
    yList = []
    
    #loop to create a clearable list of length 500, and add values to it
    for i in range(listMin, listMax, 1000):
        #create the list item
        cl = Clearable(500)
        
        xList.append(i)
        
        #allow time for garbage collection
        time.sleep(1)
        
        startTime = time.perf_counter()
        #append the test number of items to the list
        for j in range(i):
            cl.append(1)
            
        endTime = time.perf_counter()
        
        deltaT = (endTime - startTime)
        
        yList.append(deltaT*1000000)

        
    #add all this info to our xml file
    
    file.write('  <Sequence title="Time for append for 500 item list" color="green"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    file.write('</Plot>\n')
    
    #redo the test with 5000 list
    xList = []
    yList = []
    
    #loop to create a clearable list of length 999, and add values to it
    for i in range(listMin, listMax, 1000):
        #create the list item
        cl = Clearable(999)
        
        xList.append(i)
        
        #allow time for garbage collection
        time.sleep(1)
        
        startTime = time.perf_counter()
        #append the test number of items to the list
        for j in range(i):
            cl.append(1)
            
        endTime = time.perf_counter()
        
        deltaT = (endTime - startTime)
        
        yList.append(deltaT*1000000)

        
    #add all this info to our xml file
    
    file.write('  <Sequence title="Time for append for 999 item list" color="red"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    file.write('</Plot>')
    file.close
    
    
if __name__ == '__main__':
    test()

'''
cl = Clearable(10)
cl.append('oi')
cl.append('thar')
cl.append('oi')
cl.append('thar')
cl.append('oi')
cl.append('thar')
cl.append('oi')
cl.append('thar')
cl.append('oi')
cl.append('new')

print(cl['new'])
print(cl['oi'])
print(cl['not'])




print(cl.fixedList)
'''

