'''
Created on Mar 11, 2019

@author: robzo
'''

import time
import random
import string
#Requirement: Create a test to discover the complexity of 
#string compares in python

def main():
    #write an xml file with the results
    file = open("StringCompareTiming.xml","w")
    
    file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    
    file.write('<Plot title="String Compare time for Random Strings">\n')

    stringMin = 1000
    stringMax = 20000

    xList = []
    yList = []
    str1List = []
    str2List = []

    #case: random strings

    #loop creating two random strings, comparing them, then repeating with 
    #larger strings
    for i in range(stringMin, stringMax, 1000):
        xList.append(i)
        #create the string
        for j in range(i-1):
            str1List.append(random.choice(string.ascii_letters))
            str2List.append(random.choice(string.ascii_letters))
    
        #join the random word lists into strings
        str1 = ''.join(str1List)
        str2 = ''.join(str2List)
    
        #allow for garbage collection    
        time.sleep(1)
    
        #Measure the time it takes to compare 2 strings
        startTime = time.perf_counter()
        x = str1 == str2
        endTime = time.perf_counter()
        deltaT = (endTime - startTime)
        #add the time value to the y axis in microseconds
        yList.append(deltaT * 1000000)
        
    
    #write the list values into the file
    file.write('  <Axes>\n')
    file.write('   <XAxis min="'+str(stringMin)+'" max="'+str(stringMax)+'">String Size</XAxis>\n')
    file.write('   <YAxis min="'+str(min(yList))+'" max="'+str(40)+'">Compare Time</YAxis>\n')
    file.write('  </Axes>\n')
    
    file.write('  <Sequence title="Time for random string compare vs string length" color="red"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    
    #clear the lists
    xList = []
    yList = []
    str1List = []
    str2List = []
    
    #worst case: all string elements are the same
    for i in range(stringMin, stringMax, 1000):
        xList.append(i)
        
        #create one list of characters
        for j in range(i-1):
            v = random.choice(string.ascii_letters)
            str1List.append(v)
            
        #use the list to create 2 identical strings
        str1 = ''.join(str1List)
        str2 = ''.join(str1List)
        
        #allow for garbage collection    
        time.sleep(1)
    
        #Measure the time it takes to compare 2 strings
        startTime = time.perf_counter()
        #dummy operation
        x = str1 == str2
        endTime = time.perf_counter()
        deltaT = (endTime - startTime)
        #add the time value to the y axis in microseconds
        yList.append(deltaT * 1000000)
        
    file.write('  <Sequence title="Time for identical string compare vs string length" color="blue"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    
    #reset the lists
    xList = []
    yList = []
    str1List = []
    str2List = []
    
    #best case: all list elements different
    for i in range(stringMin, stringMax, 1000):
        xList.append(i)
        
        #create one list of characters
        for j in range(i-1):
            str1List.append('a')
            str2List.append('b')
            
        #use the list to create 2 completely different strings
        str1 = ''.join(str1List)
        str2 = ''.join(str2List)
        
        #allow for garbage collection    
        time.sleep(1)
    
        #Measure the time it takes to compare 2 strings
        startTime = time.perf_counter()
        #dummy operation
        x = str1 == str2
        endTime = time.perf_counter()
        deltaT = (endTime - startTime)
        #add the time value to the y axis in microseconds
        yList.append(deltaT * 1000000)
        
    file.write('  <Sequence title="Time for different string compare vs string length" color="green"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    file.write('</Plot>\n')
    print('done')
    file.close
    
        
        
        
if __name__ == "__main__":
    main()