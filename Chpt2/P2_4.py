'''
Created on Mar 13, 2019

@author: robzo
'''

import time, random

#requirement: create a program that measures the time to find the index of a list member
#with lists of various lengths, vs the built in index method

def main():
    #write an xml file with the results
    file = open("ListIndexCompareTiming.xml", "w")
    
    file.write('<?xml version="1.0" encoding="UTF-8" standalone="no" ?>\n')
    
    file.write('<Plot title="List Index search time for Random Lists">\n')
    
    #min and max list lengths
    listMin = 1000
    listMax = 20001
    
    xList = []
    yList = []
    indList = []
    
    #loop to create a list of various lengths, and randomly find a number of values from it
    for i in range(listMin, listMax, 1000):
        #total time for 1000 trials
        totalTime = 0
        indList = []
        
        xList.append(i)
        #create the list
        for j in range(i):
            indList.append(j)
        
        #allow time for garbage collection
        time.sleep(1)
        
        #try to find 1000 
        for j in range(1000):
            #create a random int in range of the list
            x = random.randint(0,i-1)
            startTime = time.perf_counter()
            #dummy assignment
            y = findInList(x, indList)
            endTime = time.perf_counter()
            deltaT = (endTime - startTime)
            #add the time taken to the total time
            totalTime += deltaT
        
        #add the total time to the yList for plotting, 1000 for ms and 1000 trials
        yList.append(totalTime*1000)
        
    #add all this info to our xml file
    
    file.write('  <Sequence title="Time for loop index search vs list length" color="blue"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
        
    #now redo the list search but using the index method
    
    #reset our lists
    xList = []
    yList = []
    indList = []
    
    #loop to create a list of various lengths, and randomly find a number of values from it
    for i in range(listMin, listMax, 1000):
        #total time for 1000 trials
        totalTime = 0
        indList = []
        
        xList.append(i)
        #create the list
        for j in range(i):
            indList.append(j)
        
        #allow time for garbage collection
        time.sleep(1)
        
        #try to find 1000 
        for j in range(1000):
            #create a random int in range of the list
            x = random.randint(0,i-1)
            startTime = time.perf_counter()
            #dummy assignment
            y = indList.index(x)
            endTime = time.perf_counter()
            deltaT = (endTime - startTime)
            #add the time taken to the total time
            totalTime += deltaT
        
        #add the total time to the yList for plotting, 1000 for ms and 1000 trials
        yList.append(totalTime*1000)
        
    #add all this info to our xml file
    
    file.write('  <Sequence title="Time for index method search vs list length" color="red"\n')
    
    for i in range(len(xList)):
        file.write('   <DataPoint x="'+str(xList[i])+'" y="'+str(yList[i])+'"/>\n')
    
    file.write('  </Sequence>\n')
    file.write('</Plot>\n')
    file.close

def findInList(val, l):
    #returns the first index of the passed value in list l
    for i in range(len(l)):
        if val == l[i]:
            return i
    print('Value not in list')
    print('Value:'+str(i))
    return None

if __name__ == '__main__':
    main()
